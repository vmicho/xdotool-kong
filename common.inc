#!/usr/bin/env bash

#
# Common stuff (init, functions, ...) for autoclicking
#


#-- Get the environment variable 'ZERO' for zero point if given. Otherwise use Z
if [ "$ZERO" != "" ]; then
    echo "INIT: using ZERO='$ZERO' as zero point"
    Z="$ZERO"
fi


#-- Check if we have xdotool
if ! which xdotool; then
    echo "INIT: ERROR: xdotool not found. Install the package 'xdotool' and try again."
    return 1
fi


#-- get the window ID
WID=$(xdotool search --onlyvisible --name "$WINDOW_TITLE" | head -1)
if [ "$WID" = "" ]; then
    echo "ERROR: window '$WINDOW_TITLE' not found"
    return 1
fi
echo "INIT: Zero point Z='$Z', Window ID WID='$WID'"


#-- make sure TEST is set. If not given, use TEST=1
if [ "$TEST" = "" ]; then
    TEST=1
fi


#-- move the cursor to coordinates relative to the 0 of the window
#   MOVE [--quiet] <x> <y> [<jitter x> <jitter y>]
MOVE()
{
    declare -i jitterArgx=0
    declare -i jitterArgy=0
    declare -i jitterx=0
    declare -i jittery=0
    declare -i posx
    declare -i posy
    declare beQuiet=0

    if [ "$1" = "--quiet" ]; then
        beQuiet=1
        shift
    fi

    if [ -n "$3" ] && [ -n "$4" ]; then
        jitterArgx=$3
        jitterArgy=$4
        if [ $jitterArgx -ge 1 ]; then
            (( jitterx = ( RANDOM % jitterArgx + RANDOM % jitterArgx ) / 2 - jitterArgx/2 ))
        fi
        if [ $jitterArgy -ge 1 ]; then
            (( jittery = ( RANDOM % jitterArgy + RANDOM % jitterArgy ) / 2 - jitterArgy/2 ))
        fi
    fi

    ((posx = $1 + jitterx))
    ((posy = $2 + jittery))

    if [ $beQuiet = 0 ]; then
        echo "    MOVE($1 $2 $3 $4): x=$posx, y=$posy, jitterx=$jitterx, jittery=$jittery"
    fi

    xdotool mousemove --window $WID $Z mousemove_relative --sync $posx $posy
}

#-- move the cursor relative to last position
MOVE_REL()
{
    xdotool mousemove_relative --sync -- $1 $2
}

#-- click at the current position
CLICK()
{
    xdotool click --delay 50 1
}

#-- press 's' key
PRESS_S()
{
    xdotool key s
}

#-- press 'Arrow Right' key
PRESS_RIGHT()
{
    xdotool key Right
}

#-- press 'spacebar' key
PRESS_SPACE()
{
    xdotool key space
}

#-- press a given key
PRESS_KEY()
{
    xdotool key "$1"
}


#-- millisecond sleep +/- 33% random variation
#   usage: SLEEP <ms> [--quiet]
SLEEP()
{
    declare -i ms=100 #-- milliseconds to sleep
    declare sr
    declare -i msr #-- randomness to add to $ms
    declare quiet=0

    while [ $# -ge 1 ]; do
        if [ "$1" = "--quiet" ]; then
            quiet=1
        else
            ms=$1
        fi
        shift
    done

    if [ $ms -lt 10 ]; then
        ms=10
    fi
    msr=$((ms + RANDOM%(ms*2/3) - ms/3))
    sr=$(echo "scale=3; $msr/1000" | bc)
    if [ $quiet = 0 ]; then
        echo "    SLEEP($ms): sleeping $sr s ($msr ms)"
    fi
    sleep $sr
}


#-- Activate the window and move to zero point
ACTIVATE_AND_ZERO()
{
    xdotool windowactivate $WID mousemove --window $WID --sync $Z
}


#-- Move smoothly with <steps> steps, with <delay> ms between steps, from <pos1> to <pos2>
#   <pos1> and <pos2> are in the format "x y jitterx jittery".
#   <pos1> will not be moved to (intended as step 0, but will be skipped, so we do not move the cursor again on the starting position)
#   we will always reach <pos2>
#
# > MOVE_SMOOTHLY <steps> <delay> <x1> <y1> <jitterx1> <jittery1> <x2> <y2> <jitterx2> <jittery2>
MOVE_SMOOTHLY()
{
    declare -i steps=$1
    declare -i delay=$2
    shift 2
    declare -i x1=$1
    declare -i y1=$2
    declare -i jx1=$3
    declare -i jy1=$4
    declare -i x2=$5
    declare -i y2=$6
    declare -i jx2=$7
    declare -i jy2=$8
    declare -i x
    declare -i y
    declare -i jx
    declare -i jy
    declare -i step

    for (( step=1; step<=steps; step++ )); do
        (( x = x1 + (x2-x1)*step/steps ))
        (( y = y1 + (y2-y1)*step/steps ))
        (( jx = jx1 + (jx2-jx1)*step/steps ))
        (( jy = jy1 + (jy2-jy1)*step/steps ))
        MOVE --quiet $x $y $jx $jy
        SLEEP $delay --quiet
    done

}
