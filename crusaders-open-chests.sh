#!/bin/bash

# usage:
#
# Edit the crusaders.inc as needed (the most important variable is Z: zero of the game. Will change if you resize the window.)
# Start the game in a browser (Only one!)
# Activate the screen where you open chests
# The chest column to use is defined by CHEST_INDEX (0=leftmost, 4=rightmost)

# Usage:
#
# Test the zero point:
# > TEST=1 ./crusaders-open-chests.sh
# If the cursor is not exactly (+/- 1-3 pixels) at the top-left corner of the game, readjust the coords in crusaders.inc.
#
# Test the chest selection (hover the mouse over the 'open 100x' button):
# > TEST=2 ./crusaders-open-chests.sh
#
# Start the real script, with 10 cycles, chest index 2 (middle):
# > TEST=0 CYCLES=10 CHEST_INDEX=2 ./crusaders-open-chests.sh
#

# Run time: 1 cycle = ~1 minute


if [ "$CYCLES" = "" ]; then
    CYCLES=1 #-- how many times to repeat the whole enhance cycle
fi

if [ "$CHEST_INDEX" = "" ]; then
    CHEST_INDEX=2 #-- index of the chest to enhance (0..4, 2 is the midlle one)
fi


#-- include crusaders-specific stuff
. crusaders.inc

#-- include common stuff
. common.inc

if [ $? != 0 ]; then
    echo "ERROR: initialization failed. Exiting"
    exit 1
fi

set -u


#-- Fixed values
DX_BUY_CHESTS=176
OFFSET_BUY_CHESTS=113
COORD_BUY_100_CHESTS="$((OFFSET_BUY_CHESTS+DX_BUY_CHESTS*CHEST_INDEX)) 668 21 11" #-- button to buy 100 chests
COORD_BUY_CONFIRM="430 370 61 15" #-- confirm button to buy
DELAY_WAIT_FOR_100_CHESTS=40 #-- how long (seconds) to wait for all 100 chests to open
COORD_CLOSE_LIST="917 30 11 11" #-- close button (chests outcome screen)


echo "---"
echo "CYCLES=$CYCLES"
echo "CHEST_INDEX=$CHEST_INDEX"
echo "COORD_BUY_100_CHESTS=$COORD_BUY_100_CHESTS"
echo "---"


TIME()
{
    echo -n $(date "+%s")
}






#--
#-- MAIN
#--

echo "---"
ACTIVATE_AND_ZERO
echo "---"

#-- Test runs

# Test 1: Just activate the window (already done) and exit
if [ $TEST = 1 ]; then
    exit 0
fi

# Test 2: Hover over the 100x buy button
if [ $TEST = 2 ]; then
    sleep 1
    SLEEP 300
    for ((i=0;i<20;i++)); do
        MOVE $COORD_BUY_100_CHESTS
        sleep 0.1
    done
    exit 0
fi


timeAll="Times and cycles:\n    Start: $(TIME)"

#-- repeat all CYCLES times
for (( cycleCount=0; cycleCount<CYCLES; cycleCount++ )); do
echo "*** BEGIN CYCLE $((cycleCount+1)) / $CYCLES"


#-- click on the 100x buy button
MOVE $COORD_BUY_100_CHESTS
SLEEP 400
CLICK

#-- confirm buy
MOVE $COORD_BUY_CONFIRM
SLEEP 2000
CLICK

#-- wait and close
echo "sleeping for $DELAY_WAIT_FOR_100_CHESTS seconds"
sleep $DELAY_WAIT_FOR_100_CHESTS

for ((i=0;i<10;i++)); do
    MOVE $COORD_CLOSE_LIST
    SLEEP 500
    CLICK
done

echo "cycle done, sleeping for 5 seconds"
SLEEP 5000

timeAll="$timeAll\n    Cycle $((cycleCount+1))/$CYCLES end: $(TIME)"
echo -e "$timeAll"

done    #-- done cycleCount
