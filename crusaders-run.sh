#!/bin/bash

# usage:
#
# Edit the crusaders.inc as needed (the most important variable is Z: zero of the game. Will change if you resize the window.)
# Start the game in a browser (Only one!)
# Activate the tab with heroes (should be on by default)
# The character to be enhanced must be on the top when HERO_INDEX_Y=0 or bottom when HERO_INDEX_Y=1  (will be clicked at that position)
#   Similar with HERO_INDEX_X=0 is leftmost ... HERO_INDEX_X=2 is rightmost

# Usage:
#
# Test the zero point:
# > TEST=1 ./crusaders-run.sh
# If the cursor is not exactly (+/- 1-3 pixels) at the top-left corner of the game, readjust the coords in crusaders.inc.
#
# Test hover over the coin collection area, then position the cursor over the hero to enhance:
# > TEST=2 ./crusaders-run.sh

# Start the script:
# > TEST=0 CYCLES=10000 ./crusaders-run.sh
#
# Start with all (try storm once every 10 min):
# > TEST=0 CYCLES=20000 RIGHT_INTERVAL=200 STORM_DELAY=1 STORM_INTERVAL=600 GO_DELAY=4000 GO_INTERVAL=1000 ./crusaders-run.sh
#
# Mode selection:
# MODE=1 (default) : enhance the given hero
# MODE=2 : enhance all heroes (must disable button confirmation!) + all upgrades
# MODE=3 : speed run (enhance as in MODE=2, press repeatedly RIGHT, hover over quickly, click when hover is on the right or left)
# MODE=4 : speed advance only (no enhances, press repeatedly RIGHT, hover over quickly) - just advance fast to an area where there is no more progress


# TODO: PROCESS_SKILLS(): parametrize. Currently hardcoded, always active.

# TODO: REFACTOR: storm rider and other stuff is not very compatible with MODE 3.
#       Need to do all the button stuff during hover -> just activate a state variable that says we are in 'storm' mode and
#       press the buttons during hover or during the main loop

# DONE: remove goldorama, pretty useless

# DONE: Magnify+Right cooloff before Storm as Gold-o-rama can steal magnify -> need to do Storm in real intervals
#      + can also use mag to gold. Workaround: disable gold.
#      -> removed gold-o-rama completely


# Run time: ????? 1 cycle = ~1m30s  -> see how much cycles is done in a specific mode in some time ...


if [ "$CYCLES" = "" ]; then
    CYCLES=1 #-- how many times to repeat the whole enhance cycle
fi

if [ "$MODE" = "" ] || [ "$MODE" = "1" ] || [ "$MODE" = "0" ]; then
    MODE=1 #-- enhance the given hero
elif [ "$MODE" = "3" ]; then
    MODE=3 #-- speed run
elif [ "$MODE" = "4" ]; then
    MODE=4 #-- speed advance
else
    MODE=2 #-- enhance all heroes
fi


if [ "$RIGHT_INTERVAL" = "" ]; then
    RIGHT_INTERVAL=0    #-- Press RIGHT key interval. 0=disabled
fi

if [ "$STORM_INTERVAL" = "" ]; then
    STORM_INTERVAL=0    #-- Storm rider (with magnify) interval. 0=disabled
fi

if [ "$STORM_DELAY" = "" ]; then
    #-- Storm rider initial delay: 1st Storm begins after this number, then STORM_INTERVAL is used. 0=disabled
    STORM_DELAY=0
fi

if [ "$GO_INTERVAL" = "" ]; then
    GO_INTERVAL=0    #-- "G" key interval. 0=disabled
fi
if [ "$GO_DELAY" = "" ]; then
    GO_DELAY=0    #-- "G" key initial delay. 0=disabled
fi


#-- include crusaders-specific stuff
. crusaders.inc

#-- include common stuff
. common.inc

if [ $? != 0 ]; then
    echo "ERROR: initialization failed. Exiting"
    exit 1
fi


#-- Fixed values
COORD_ENHANCE_HERO_X0="300"
COORD_ENHANCE_HERO_X1="615"
COORD_ENHANCE_HERO_X2="930"
COORD_ENHANCE_HERO_Y0="530"
COORD_ENHANCE_HERO_Y1="614"

if [ "$HERO_INDEX_X" = "" ]; then
    HERO_INDEX_X=0
fi
if [ "$HERO_INDEX_Y" = "" ]; then
    HERO_INDEX_Y=0
fi
if [ "$HERO_INDEX_Y" = "1" ]; then
    COORD_ENHANCE_HERO_Y="$COORD_ENHANCE_HERO_Y1"
else
    COORD_ENHANCE_HERO_Y="$COORD_ENHANCE_HERO_Y0"
fi
if [ "$HERO_INDEX_X" = "2" ]; then
    COORD_ENHANCE_HERO_X="$COORD_ENHANCE_HERO_X2"
elif [ "$HERO_INDEX_X" = "1" ]; then
    COORD_ENHANCE_HERO_X="$COORD_ENHANCE_HERO_X1"
else
    COORD_ENHANCE_HERO_X="$COORD_ENHANCE_HERO_X0"
fi
COORD_ENHANCE_HERO="$COORD_ENHANCE_HERO_X $COORD_ENHANCE_HERO_Y"

JITTER_ENHANCE_HERO="87 34"
HOVER_X1=650
HOVER_Y1=220
HOVER_X2=910
HOVER_Y2=420
HOVER_STEP_X=20
HOVER_STEP_Y=2
GOLD_CLICK_POSITION="350 450 3 3"
COORD_ENHANCE_ALL_HEROES="985 637 13 45" #-- green button to enhance max. levels
COORD_UPGRADE_ALL_HEROES="985 542 13 45" #-- blue button to get max. upgrades


export NC='\e[0m' # No Color
export COLOR_WHITE='\e[1;37m'
export COLOR_BLACK='\e[0;30m'
export COLOR_BLUE='\e[0;34m'
export COLOR_LIGHT_BLUE='\e[1;34m'
export COLOR_GREEN='\e[0;32m'
export COLOR_LIGHT_GREEN='\e[1;32m'
export COLOR_CYAN='\e[0;36m'
export COLOR_LIGHT_CYAN='\e[1;36m'
export COLOR_RED='\e[0;31m'
export COLOR_LIGHT_RED='\e[1;31m'
export COLOR_PURPLE='\e[0;35m'
export COLOR_LIGHT_PURPLE='\e[1;35m'
export COLOR_BROWN='\e[0;33m'
export COLOR_YELLOW='\e[1;33m'
export COLOR_GRAY='\e[0;30m'
export COLOR_LIGHT_GRAY='\e[0;37m'


TIME()
{
    echo -n $(date "+%s")
}

#-- press right only in MODE=3 or MODE=4
MODE3_PRESS_RIGHT()
{
    if [[ $MODE = 3 || $MODE = 4 ]]; then
        PRESS_RIGHT
        #echo "                  MODE3/4 RIGHT"
    fi
}

#-- click in MODE 3
MODE3_CLICK()
{
    if [ $MODE = 3 ]; then
        CLICK
        #echo "                  MODE3 CLICK"
    fi
}

HOVER()
{
    declare -i x=$HOVER_X1
    declare -i y=$HOVER_Y1
    declare -i dx=$HOVER_STEP_X
    declare -i dy=$HOVER_STEP_Y
    MOVE $x $y 15 15
    MODE3_PRESS_RIGHT
    MODE3_CLICK
    while true; do
        MOVE_REL $dx $dy
        sleep 0.008
        ((x+=dx))
        ((y+=dy))
        if [ $dx -ge 0 ] && [ $x -ge $HOVER_X2 ]; then
            dx=-$HOVER_STEP_X
            MODE3_PRESS_RIGHT
            MODE3_CLICK
        fi
        if [ $dx -lt 0 ] && [ $x -le $HOVER_X1 ]; then
            dx=$HOVER_STEP_X
            MODE3_PRESS_RIGHT
            MODE3_CLICK
        fi
        if [ $dy -ge 0 ] && [ $y -ge $HOVER_Y2 ]; then
            break #-- do not go back up
            dy=-$HOVER_STEP_Y
        fi
        if [ $dy -lt 0 ] && [ $y -le $HOVER_Y1 ]; then
            break
        fi
    done
}


#-- process storm rider (SR), non-blocking (will delay about 500 ms each call when SR is active, 0 when not active)
#   Will activate itself when needed and be active for 12 times (about 6-7 s)
stormRiderIndex=-1
PROCESS_STORM_RIDER()
{
    #-- Time for Storm rider?
    echo "  *--- STORM state: time=$timeDiffStorm s / STORM_DELAY=$STORM_DELAY s / STORM_INTERVAL=$STORM_INTERVAL s, index=$stormRiderIndex"
    if [[ ( $STORM_DELAY -gt 0  && $timeDiffStorm -ge $STORM_DELAY ) || ( $STORM_DELAY = 0 && $STORM_INTERVAL -gt 0  && $timeDiffStorm -ge $STORM_INTERVAL ) ]]; then
        echo "  *    -> $timeDiffStorm >= $STORM_DELAY or $STORM_INTERVAL, activating Storm"
        stormRiderIndex=0
        timeLastStorm=$(TIME)
        timeDiffStorm=0
        STORM_DELAY=0   #-- from now, only STORM_INTERVAL will matter.
    fi

    #-- process SR if activated (stormRiderIndex > 0)
    if [[ $stormRiderIndex -ge 0  &&  $stormRiderIndex -lt 12 ]]; then
        echo -e "  * ${COLOR_LIGHT_CYAN}STORM RIDER ACTIVE${NC}"
        #-- To activate more or less correctly, we need to press 2 (magnify) and 7 (storm rider) hotkeys for about
        #   6 seconds as we might already be in a screen transition (abilities are disabled during that time)
        #   Note: We may as well be in a state of heavy beating. Well we try in next interval in few minutes and hope
        #         for the best :]
        #   Note: This methos is not perfect as the '2' key can be pressed during screen transition and SR will not
        #         be magnified, but there is only a small chance for this to happen.

        echo -n -e "  * Pressing '2' and '7' for 12 cycles ( i = ${COLOR_LIGHT_CYAN}$stormRiderIndex / 12${NC} ):"
        PRESS_KEY 2
        echo -n " [2]"
        SLEEP --quiet 50
        PRESS_KEY 7
        echo " [7]"
        SLEEP --quiet 400
        MODE3_PRESS_RIGHT

        ((stormRiderIndex++))
        echo "  *---"
    elif [ $stormRiderIndex -ge 0 ]; then
        #-- stormRiderIndex reached the maximum
        stormRiderIndex=-1
        echo "  *---"
    fi
}


skillsIndex=-1
SKILLS_INTERVAL=58
PROCESS_SKILLS()
{
    #-- Time for skills?
    echo "  *--- SKILLS state: time=$timeDiffSkills s / SKILLS_INTERVAL=$SKILLS_INTERVAL s, index=$skillsIndex"
    if [[ $SKILLS_INTERVAL -gt 0  && $timeDiffSkills -ge $SKILLS_INTERVAL ]]; then
        echo "  *    -> activating Skills"
        skillsIndex=0
        timeLastSkills=$(TIME)
        timeDiffSkills=0
    fi

    #-- process skills if activated (skillsIndex > 0)
    if [[ $skillsIndex -ge 0  &&  $skillsIndex -lt 10 ]]; then
        echo -e "  * ${COLOR_LIGHT_CYAN}SKILLS ACTIVE${NC}"

        echo -n -e "  * Pressing '1' '2' '3' '4' '5' '6' '8' ( i = ${COLOR_LIGHT_CYAN}$skillsIndex${NC} ):"
        PRESS_KEY 1
        echo -n " [1]"
        SLEEP --quiet 30
        PRESS_KEY 2
        echo -n " [2]"
        SLEEP --quiet 30
        PRESS_KEY 3
        echo -n " [3]"
        SLEEP --quiet 50
        PRESS_KEY 4
        echo -n " [4]"
        SLEEP --quiet 40
        PRESS_KEY 5
        echo -n " [5]"
        SLEEP --quiet 40
        PRESS_KEY 6
        echo -n " [6]"
        SLEEP --quiet 30
        PRESS_KEY 8
        echo " [8]"
        SLEEP --quiet 300

        ((skillsIndex++))
        echo "  *---"
    elif [ $skillsIndex -ge 0 ]; then
        #-- index reached the maximum
        skillsIndex=-1
        echo "  *---"
    fi
}





#--
#-- MAIN
#--

echo "---"
ACTIVATE_AND_ZERO
echo "TEST=$TEST"
echo "---"

#-- Test runs

# Test 1: Just activate the window (already done) and exit
if [ $TEST = 1 ]; then
    exit 0
fi

# Test 2: Hover over the coin collection area, then position the cursor over the hero to enhance
if [ $TEST = 2 ]; then
    sleep 1
    HOVER
    SLEEP 300
    if [ $MODE = 1 ]; then
        for ((i=0;i<20;i++)); do
            MOVE $COORD_ENHANCE_HERO $JITTER_ENHANCE_HERO
            sleep 0.1
        done
    elif [ $MODE = 2 ] || [ $MODE = 3 ]; then
        for ((i=0;i<30;i++)); do
            if [ $i -lt 15 ]; then
                MOVE $COORD_ENHANCE_ALL_HEROES
            else
                MOVE $COORD_UPGRADE_ALL_HEROES
            fi
        sleep 0.2
        done
    fi
    exit 0
fi



#-- Normal run

timeLast=$(TIME)
echo "* timeLast=$timeLast"
timeLastRight=$timeLast
timeLastStorm=$timeLast
timeLastSkills=$timeLast
timeLastEnhanceAll=$timeLast
timeLastUpgradeAll=$timeLast
timeLastGo=$timeLast

# repeat all CYCLES times
for (( cycleCount=0; cycleCount<CYCLES; cycleCount++ )); do
echo "*** BEGIN CYCLE $((cycleCount+1)) / $CYCLES"

timeNow=$(TIME)
((timeDiffRight=timeNow-timeLastRight))
((timeDiffStorm=timeNow-timeLastStorm))
((timeDiffSkills=timeNow-timeLastSkills))
((timeDiffEnhanceAll=timeNow-timeLastEnhanceAll))
((timeDiffUpgradeAll=timeNow-timeLastUpgradeAll))
((timeDiffGo=timeNow-timeLastGo))

#-- wave the mouse over the action area like we don't care
HOVER


#-- enhance the given hero(es)
#-- MODE=1 : enhance the given hero
if [[ $MODE = 1 ]]; then
    MOVE $COORD_ENHANCE_HERO $JITTER_ENHANCE_HERO
    SLEEP 200
    CLICK
    SLEEP 300
fi


#-- combined loop for enhancing
#-- MODE=2 or MODE=3 : enhance all + upgrade all heroes. Do it every 37 / 120 seconds.
#   Be quicker if MODE=3 (hover only once over the enhance buttons, otherwise (MODE=2) 3x)

modeIndexLoops=6
modeIndexSplit=3
if [ $MODE = 3 ]; then
    modeIndexLoops=2
    modeIndexSplit=1
fi

for (( modeIndex=0; modeIndex<modeIndexLoops; modeIndex++ )); do
    if [[ $MODE = 2 || $MODE = 3 ]] ; then
        if [[ $modeIndex -lt $modeIndexSplit ]]; then
            #-- hover over Enhace All button, click it if enough timeDiffEnhanceAll elapsed
            echo -e "  * hover over Enhance All (index=$modeIndex, loops=$modeIndexLoops, split=$modeIndexSplit, MODE=$MODE)"
            MOVE $COORD_ENHANCE_ALL_HEROES
            if [[ $timeDiffEnhanceAll -ge 37 ]]; then
                echo -e "  * ${COLOR_LIGHT_RED}All enhance click${NC}"
                CLICK
                timeDiffEnhanceAll=0
                timeLastEnhanceAll=$(TIME)
            fi
            if [ $MODE = 2 ]; then
                SLEEP --quiet 500
            else
                SLEEP --quiet 200
                MODE3_PRESS_RIGHT
            fi
        else
            #-- hover over Upgrade All button, click it if enough timeDiffUpgradeAll elapsed
            echo -e "  * hover over Upgrade All"
            MOVE $COORD_UPGRADE_ALL_HEROES
            if [[ $timeDiffUpgradeAll -ge 120 ]]; then
                echo -e "  * ${COLOR_LIGHT_RED}All upgrades click${NC}"
                CLICK
                timeDiffUpgradeAll=0
                timeLastUpgradeAll=$(TIME)
            fi
            if [ $MODE = 2 ]; then
                SLEEP --quiet 400
            else
                SLEEP --quiet 200
                MODE3_PRESS_RIGHT
            fi
        fi

    fi
    PROCESS_STORM_RIDER
    PROCESS_SKILLS
done    #-- for modeIndex



#-- Time for press RIGHT?
echo "  * timeDiffRight=$timeDiffRight s / RIGHT_INTERVAL=$RIGHT_INTERVAL s"
if [[ $RIGHT_INTERVAL -gt 0 && $timeDiffRight -ge $RIGHT_INTERVAL ]]; then
    echo "    -> >= $RIGHT_INTERVAL, pressing RIGHT"
    PRESS_RIGHT
    timeLastRight=$(TIME)
fi


#-- T-ime for press "G" (go)?
echo "  * timeDiffGo=$timeDiffGo s / GO_INTERVAL=$GO_INTERVAL s, GO_DELAY=$GO_DELAY s"
if [[ ( $GO_DELAY -gt 0  && $timeDiffGo -ge $GO_DELAY ) || ( $GO_DELAY = 0 && $GO_INTERVAL -gt 0 && $timeDiffGo -ge $GO_INTERVAL ) ]]; then
    echo "    -> $timeDiffGo >= $GO_DELAY or $GO_INTERVAL, activating Go"
    PRESS_KEY g
    timeLastGo=$(TIME)
    GO_DELAY=0   #-- from now, only GO_INTERVAL will matter.
fi


PROCESS_STORM_RIDER


#-- make a slightly bigger pause every 10 cycles
#   * except MODE=3 or MODE=4: no pause when in speed mode
if [[ $(( (cycleCount+1) % 10 )) = 0 ]]; then
    echo "  * MID CYCLE PAUSE $((cycleCount+1)) / $CYCLES"
    if [[ $MODE != 3 && $MODE != 4 ]]; then
        SLEEP 4000
    else
        echo "    -> just kidding, MODE=3/4, hurry hurry!"
    fi
fi

done    #-- done cycleCount
