#!/usr/bin/env bash

#-- Set me:
Z="1 1"     #-- zero of the game, relative to the browser window
#Z="19 328"     #-- zero of the game, relative to the browser window
HERO_INDEX_X=0   #-- 0=left hero, 1=middle hero, 2=right hero
HERO_INDEX_Y=0   #-- 0=top hero, 1=bottom hero
#WINDOW_TITLE="Play Crusaders of the Lost Idols"     #-- The browser window title we will use. As seen on the browser window.
WINDOW_TITLE="Crusaders of the Lost Idols"     #-- The standalone window title we will use. As seen on the browser window.

echo "* crusaders.inc: Zero point Z='$Z', HERO_INDEX='$HERO_INDEX'"

