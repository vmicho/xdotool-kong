#!/bin/bash

# Test that the correct window is activated and the mouse correctly positioned to the top left corner of the Time World frame.
# Zero point can be given in the Z env variable. E.g.: Z="145 296" ./thisCommand.sh

if [ "$Z" = "" ]; then
    Z="145 296"
fi
WID=$(xdotool search --onlyvisible --name "Play Time World" | head -1)
echo "* Zero point Z='$Z', WID='$WID'"
xdotool windowactivate $WID mousemove --window $WID --sync $Z

