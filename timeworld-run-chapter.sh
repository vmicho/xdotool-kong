#!/bin/bash

# Edit the timeworld.inc as needed (the most important variable is Z: zero of the game.)
# Start the game in a browser (Only one!)
# Go to chapter fight screen
# Run this script

# Usage:
#
# Test the zero point:
# > TEST=1 ./timeworld-run-chapter.sh
# or without the 'TEST=1'
# If the cursor is not exactly (+/- 1-3 pixels) at the top-left corner of the game, readjust the coords in *.inc file.
#
# Start the script:
# > TEST=0 CYCLES=200 ./timeworld-run-chapter.sh


if [ "$CYCLES" = "" ]; then
    CYCLES=1 #-- how many times to repeat the whole cycle
fi
#-- include *.inc file
. timeworld.inc
#-- include common stuff
. common.inc
if [ $? != 0 ]; then
    echo "ERROR: initialization failed. Exiting"
    exit 1
fi

#-- Fixed values
BUTTON_ATTACK="680 176 61 13" #-- coords and jitter of the attack button
BUTTON_CONFIRM_ATTACK="378 353 61 13" #-- coords and jitter of the confirm attack button
BUTTON_SKIP="815 39 61 13" #-- coords and jitter of the skip looking at the annoying fighting button
BUTTON_FIGHT_EXIT="815 39 61 13" #-- coords and jitter of the fight exit button

#--
#-- MAIN
#--

ACTIVATE_AND_ZERO

#-- Test runs
# Test 1: Just activate the window (already done) and exit
if [ $TEST = 1 ]; then
    exit 0
fi


#-- Normal run
# repeat all CYCLES times
for (( cycleCount=0; cycleCount<CYCLES; cycleCount++ )); do
echo "*** BEGIN CYCLE $((cycleCount+1)) / $CYCLES"

MOVE $BUTTON_ATTACK
SLEEP 100
CLICK
MOVE_SMOOTHLY 2 10 $BUTTON_ATTACK $BUTTON_CONFIRM_ATTACK
SLEEP 100
CLICK
MOVE_SMOOTHLY 2 10 $BUTTON_CONFIRM_ATTACK $BUTTON_SKIP
SLEEP 100
CLICK
MOVE $BUTTON_FIGHT_EXIT
SLEEP 100
CLICK
MOVE_SMOOTHLY 2 10 $BUTTON_ATTACK $BUTTON_CONFIRM_ATTACK
SLEEP 100
CLICK

done    #-- done cycleCount





