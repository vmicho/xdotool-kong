#!/bin/bash

# usage:
#
# Edit the timeworld.inc as needed (the most important variable is Z: zero of the game.)
# Start the game in a browser (Only one!)
# Create a pillar raid with only yourself in.
# Stay on the raid screen.
# Run this script.


# Usage:
#
# Test the zero point:
# > TEST=1 ./timeworld-run-pillar.sh
# or
# > ./timeworld-run-pillar.sh
# If the cursor is not exactly (+/- 1-3 pixels) at the top-left corner of the game, readjust the coords in *.inc file.
#
# Test moving mouse between the 2 buttons (pillar exit button and raid start button) with a little jiggling over
# the raid start buttons
# > TEST=2 ./timeworld-run-pillar.sh

# Start the script:
# > TEST=0 CYCLES=200 ./timeworld-run-pillar.sh


# Run time approx: 100 cycles = 40 seconds



if [ "$CYCLES" = "" ]; then
    CYCLES=1 #-- how many times to repeat the whole cycle
fi

#-- include *.inc file
. timeworld.inc

#-- include common stuff
. common.inc

if [ $? != 0 ]; then
    echo "ERROR: initialization failed. Exiting"
    exit 1
fi


#-- Fixed values
BUTTON_PILLAR_EXIT="815 40 81 19" #-- coords and jitter of the pillar exit button
BUTTON_RAID_START="618 419 61 13" #-- coords and jitter of the raid start button



#--
#-- MAIN
#--

ACTIVATE_AND_ZERO

#-- Test runs

# Test 1: Just activate the window (already done) and exit
if [ $TEST = 1 ]; then
    exit 0
fi

# Test 2: move mouse between the start button and exit button (not visible during raid screen),
# then jiggle around the start button a bit
if [ $TEST = 2 ]; then
    sleep 1
    MOVE_SMOOTHLY 10 10 $BUTTON_RAID_START $BUTTON_PILLAR_EXIT
    MOVE_SMOOTHLY 20 10 $BUTTON_PILLAR_EXIT $BUTTON_RAID_START
    SLEEP 200
    for ((i=0;i<20;i++)); do
        MOVE $BUTTON_RAID_START
        sleep 0.1
    done
    exit 1
fi



#-- Normal run

# repeat all CYCLES times
for (( cycleCount=0; cycleCount<CYCLES; cycleCount++ )); do
echo "*** BEGIN CYCLE $((cycleCount+1)) / $CYCLES"


MOVE $BUTTON_RAID_START
CLICK
CLICK
MOVE_SMOOTHLY 10 10 $BUTTON_RAID_START $BUTTON_PILLAR_EXIT
MOVE $BUTTON_PILLAR_EXIT
CLICK
CLICK
MOVE_SMOOTHLY 10 10 $BUTTON_PILLAR_EXIT $BUTTON_RAID_START
MOVE $BUTTON_RAID_START

done    #-- done cycleCount





