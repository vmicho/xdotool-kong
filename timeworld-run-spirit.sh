#!/bin/bash

# usage:
#
# Start TW game (Only one!)
# Create a raid team (to avoid any unwanted invites during the run), then close the raid window
# Open the Hero->Hero Spirit window
# Start the script


# TODO: randomness of clicks


# Usage:
#
# Test the zero point:
# > TEST=1 ./timeworld-run-spirit.sh
# or
# > ./timeworld-run-spirit.sh
# If the cursor is not exactly (+/- 1-3 pixels) at the top-left corner of the game, readjust the coords in *.inc file.
#
# Test select the enhance tab and select the item to be enhanced
# > TEST=2 ./timeworld-run-spirit.sh

# Start the script:
# > TEST=0 CYCLES=90 ./timeworld-run-spirit.sh

# Run time: 1 cycle = ~1m30s
# 250 cycles = 7h = L90->L96



if [ "$CYCLES" = "" ]; then
    CYCLES=1 #-- how many times to repeat the whole enhance cycle
fi

#-- include *.inc files
. timeworld.inc
. timeworld-spirit.inc

#-- include common stuff
. common.inc

if [ $? != 0 ]; then
    echo "ERROR: initialization failed. Exiting"
    exit 1
fi



#-- Optional set me (if things go wrong):
COUNT_HISTORY_PAGES=3 #-- number of pages to be filled with ingrediends
COUNT_AUTO_ADD=7 #-- how many times to click Auto Add Ingredient
COUNT_MANUAL_ADD=25 #-- how many times manually add an ingredient
COUNT_TAB_SWITCH_TRIES=15 #-- how many times to click on tab change. 1s delay between clicks.
                          #   Should prevent non-switching between enhance and spirit tabs.

#-- Fixed values
COORD_ENHANCE_ITEM0="210 263" #-- coords of the 1st item on enhance list (enhance tab)
JITTER_ENHANCE_ITEM="100 16"
DELTAY_ENHANCE_ITEM="26" #-- Y distance between items to enhance
COORD_ENHANCE_PGDOWN="273 385" #-- coords of the 'page down' button
JITTER_ENHANCE_PGDOWN="5 11"

COORD_ENHANCE_TAB="350 200" #-- coords of the Enhance tab (this and all below are relative to Z)
COORD_SPIRIT_TAB="280 200"  #-- coords of the Hero Spirit tab
JITTER_TAB="33 9"
COORD_MATERIAL="540 250" #-- coords of the center of the top left material icon
DELTAX_MATERIAL="43" #-- X distance of the next material icon
DELTAY_MATERIAL="43" #-- Y distance ---
COORD_AUTOADD="340 470" #-- coords of Auto Add igredients button
COORD_ENHANCE="450 470" #-- coords of Enhance igredients button
COORD_OK_HISTORY_FULL="440 359" #-- coords of the OK button of the history full warning
COORD_CLEAR_HISTORY="640 490" #-- coords of the Clear history button
SIZE_CLEAR_HISTORY="110 20" #-- button click range
COORD_OK_INVENTORY_FULL="$COORD_OK_HISTORY_FULL" #-- coords of the OK button of the inventory full warning
COORD_OK_CONFIRM_ENHANCE="370 345" #-- coords of the OK button of the confirm enhance dialog. This must be slightly above the ingredient button to avoid clicking it.
COORD_OK_AUTO_ADD_PROBLEM="440 345" #-- coords of the OK when 'auto add' is pressed and there is nothing to add (sometimes happen for unknown reason)



#-- select the item to enhance
#   Also set COORD_ENHANCE_ITEM to the correct coords
SELECT_ENHANCE_ITEM()
{
    declare -i -a tmpArray
    COORD_ENHANCE_ITEM="0 0"
    MOVE $COORD_ENHANCE_PGDOWN $JITTER_ENHANCE_PGDOWN

    # press INDEX_ENHANCE_PAGE times 'page down' button
    for ((i=0; i<INDEX_ENHANCE_PAGE; i++)); do
        SLEEP 400
        CLICK
    done

    # select the '1st + INDEX_ENHANCE_ITEM'-th item
    SLEEP 500
    tmpArray=( $COORD_ENHANCE_ITEM0 )
    COORD_ENHANCE_ITEM="${tmpArray[0]} $(( tmpArray[1] + DELTAY_ENHANCE_ITEM * INDEX_ENHANCE_ITEM))"
    MOVE_SMOOTHLY 10 10    $COORD_ENHANCE_PGDOWN 3 3    $COORD_ENHANCE_ITEM 5 3
    SLEEP 100
    CLICK
}

#-- do double click (safe click)
CLICK2()
{
    CLICK
    SLEEP 100 --quiet
    CLICK
}



#--
#-- MAIN
#--

ACTIVATE_AND_ZERO

#-- Test runs

# Test 1: Just activate the window (already done) and exit
if [ $TEST = 1 ]; then
    exit 0
fi

# Test 2: select the enhance tab and select the item to be enhanced
if [ $TEST = 2 ]; then
    sleep 1
    MOVE_SMOOTHLY 30 10    0 0 3 3    $COORD_SPIRIT_TAB 5 3
    SLEEP 300
    CLICK
    SLEEP 300
    MOVE_SMOOTHLY 20 10    $COORD_SPIRIT_TAB 3 3    $COORD_ENHANCE_TAB 5 3
    SLEEP 300
    CLICK
    SLEEP 300
    SELECT_ENHANCE_ITEM
    SLEEP 300
    for ((i=0;i<20;i++)); do
        MOVE $COORD_ENHANCE_ITEM $JITTER_ENHANCE_ITEM
        sleep 0.1
    done
    exit 1
fi

# Test 3: select the enhance tab / spirit tap multiple times
if [ $TEST = 3 ]; then
    for ((i=0;i<5;i++)); do
        MOVE_SMOOTHLY 20 10    $COORD_ENHANCE_TAB 5 3    $COORD_SPIRIT_TAB 5 3
        SLEEP 300
        CLICK2
        
        MOVE_SMOOTHLY 20 10    $COORD_SPIRIT_TAB 5 3    $COORD_ENHANCE_TAB 5 3
        SLEEP 300
        CLICK2
    done
    exit 1
fi

# Test 4: =test3, just not smoothly
if [ $TEST = 4 ]; then
    for (( i=0; i<10; i++ )); do
        MOVE $COORD_SPIRIT_TAB $JITTER_TAB
        SLEEP 100
        CLICK
        
        SLEEP 500
        
        MOVE $COORD_ENHANCE_TAB $JITTER_TAB
        SLEEP 100
        CLICK
        SLEEP 500
    done
    exit 1
fi



#-- normal run

# select the spirit tab first
MOVE_SMOOTHLY 20 10    0 0 3 3    $COORD_SPIRIT_TAB 5 3
SLEEP 300
CLICK2


# repeat all 
for (( cycleCount=0; cycleCount<CYCLES; cycleCount++ )); do
echo "*** BEGIN CYCLE $((cycleCount+1)) / $CYCLES"

#-- select spirit tab
for (( i=0; i<COUNT_TAB_SWITCH_TRIES; i++ )); do
    MOVE $COORD_SPIRIT_TAB $JITTER_TAB
    SLEEP 1000
    CLICK2
done

# repeat COUNT_HISTORY_PAGES times
for (( historyPage=0; historyPage<COUNT_HISTORY_PAGES; historyPage++ )); do
echo "** FILLING HISTORY PAGE $((historyPage+1)) / $COUNT_HISTORY_PAGES"

# press 40x 'S'
for (( i=0; i<40; i++ )); do
    echo -n "    press 's' ($((i+1))/40)"
    SLEEP 300
    PRESS_S
done

# press the OK in the history full / inventory full warning dialog
if [ $historyPage -lt $((COUNT_HISTORY_PAGES-1)) ]; then
    #-- history full
    echo "* CLICK OK (history full)"
    MOVE $COORD_OK_HISTORY_FULL
else
    #-- inventory full
    echo "* CLICK OK (inventory full)"
    MOVE $COORD_OK_INVENTORY_FULL
fi
SLEEP 500
CLICK
SLEEP 200    #-- click few more times just in case (found few times the dialog still active)
CLICK
SLEEP 300
CLICK

# click Clear History button
SLEEP 600
MOVE $COORD_CLEAR_HISTORY
SLEEP 200
CLICK

done #-- done historyPage


# go to Enhance tab
echo "** CLICK ENHANCE TAB"
SLEEP 500
for ((i=0;i<COUNT_TAB_SWITCH_TRIES;i++)); do
    MOVE $COORD_ENHANCE_TAB $JITTER_TAB
    SLEEP 1000
    CLICK2
done


#-- select the item to enhance
echo "* SELECT ITEM TO ENHANCE"
SELECT_ENHANCE_ITEM


# repeat COUNT_AUTO_ADD times
for (( i=0; i<COUNT_AUTO_ADD; i++ )); do
    # press Auto Add
    echo "* CLICK AUTO ADD ($((i+1)) / $COUNT_AUTO_ADD)"
    SLEEP 400
    MOVE $COORD_AUTOADD
    SLEEP 200
    CLICK

    # press OK of the auto add problem if it happens, otherwise it will block all else
    SLEEP 200
    MOVE $COORD_OK_AUTO_ADD_PROBLEM
    SLEEP 200
    CLICK

    # press Enhance
    echo "* CLICK ENHANCE"
    SLEEP 400
    MOVE $COORD_ENHANCE
    SLEEP 200
    CLICK
done

ENHANCE_WITH_CONFIRM()
{
    SLEEP 400
    MOVE $COORD_ENHANCE
    SLEEP 200
    CLICK

    SLEEP 400
    MOVE $COORD_OK_CONFIRM_ENHANCE
    SLEEP 200
    CLICK
}

# manually add COUNT_MANUAL_ADD ingredients
echo "** MANUALLY ADD '$COUNT_MANUAL_ADD' INGREDIENTS"
j=0
for (( i=COUNT_MANUAL_ADD-1; i>=0; i-- )); do
    (( x=(i%5) * DELTAX_MATERIAL ))
    (( y=(i/5) * DELTAY_MATERIAL ))
    echo "* MANUAL ADD (x=$((i%5)), y=$((i/5)))"
    SLEEP 300
    MOVE $COORD_MATERIAL
    MOVE_REL $x $y
    
    # Repeat the click several times. On 1st click, "add as material" pops up at the exact same cursor position
    # - clicking at the same spot will add the ingredient. Subsequent clicks on the same (now added) ingredient
    # will do nothing, so it is safe to repeat indefinitely (eventually).
    for((repeatClick=0; repeatClick<5; repeatClick++)); do
        SLEEP 60
        CLICK
    done
    SLEEP 100

    ((j++))
    if [ $j = 8 ]; then
        echo "* 8 INGREDIENTS ADDED, ENHANCING"
        ENHANCE_WITH_CONFIRM
        j=0
    fi
    if [ $i = 4 ]; then
        echo " * PAUSE 3 s before last 5 items ..."
        SLEEP 3000
    fi
done
if [ $j != 0 ]; then
    echo "* ENHANCING (FINAL)"
    ENHANCE_WITH_CONFIRM
    j=0
fi



# go back to Hero Spirit tab
echo "** CLICK HERO SPIRIT TAB"
SLEEP 800
MOVE $COORD_SPIRIT_TAB
SLEEP 100
CLICK
SLEEP 200
CLICK


# make a bigger pause to allow ctrl+c of this script
echo "*** END CYCLE $((cycleCount+1)) / $CYCLES"
echo "*** Cycle complete. Waiting ~10 s ..."
SLEEP 10000
echo


done #-- done cycleCount
