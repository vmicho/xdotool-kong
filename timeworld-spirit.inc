#!/usr/bin/env bash

#-- Set me:
INDEX_ENHANCE_ITEM="3" #-- relative index of the item to enhance, on the current page (0=top item .. 9=bottom item)
INDEX_ENHANCE_PAGE="1" #-- index of the page on which the item to enhance is located (0=the 1st page shown after selecting the enhance tab, 1=page2, ...)


echo "* timeworld-spirit.inc: INDEX_ENHANCE_ITEM='$INDEX_ENHANCE_ITEM', INDEX_ENHANCE_PAGE='$INDEX_ENHANCE_PAGE'"

